# Unmail

Like unzipping, but for MBOX files!

![To the extent possible under law, the authors have waived all copyright and related or neighboring rights to this work.](https://licensebuttons.net/p/zero/1.0/80x15.png "CC0")

This is an early implementation of Unmail, an MBOX attachment extractor.
The repository is intended to contain a reusable crate, acting as a backend
for frontend projects.

The idea is to create at least a GTK frontend. Hopefully some for Cocoa
and WinUI as well.
